# Macks Aperture Photography photo gallery app


## macks-aperture-gallery 

* [Name](#Name)
* [Description](#Description)
* [Visuals](#visuals)
* [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Project Status](#project-status)


## Name
macks-aperture-gallery

## Description
Python Flask website with database and user login for photography club. 
includes photo gallery where members can upload their work to the database, 
and resource database with links to helpful websites

originated as follow along tutorial Tech with Tim. 
https://youtu.be/dam0GPOAvVI


## Visuals
![image](Screenshot_of_links_page.png)

## Installation
Python Flask website requires appropriate server

## Usage
Search through database of links of value to photographers

## Roadmap
Images uploaded to our server will be renamed
with a randomized unique string and
scaled down to a maximum dimension of 1500 pixels
with original aspect ratio preserved.
(images smaller than 1500 will not be scaled up)
current file type is limited to jpg files.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
GPLv3

![GitLab](https://img.shields.io/gitlab/license/macksm3/base-Database?style=plastic&color=blue)

## Project status
Using Diane's links spreadsheet as starting point, modified it to store in database.
Form to add links now functional.
The records are all read into html table, than sort and search functions 
are implemented with dataTables.js code.
Gallery started and form to upload photos partially functional.
