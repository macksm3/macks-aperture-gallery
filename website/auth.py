from flask import Blueprint, render_template, request, flash, redirect, url_for, current_app
from PIL import Image
from .models import User, Photo, Link
import os
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from . import db
from flask_login import login_user, login_required, logout_user, current_user
from . import UPLOAD_FOLDER

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.password, password):
                flash('Logged in successfully!', category='success')
                login_user(user, remember=True)
                return redirect(url_for('views.home'))
            else:
                flash('Incorrect password, try again.', category='error')
        else:
            flash('Email does not exist.', category='error')

    return render_template("login.html", user=current_user)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('views.gallery'))


@auth.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        user_name = request.form.get('user_name')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')

        user = User.query.filter_by(email=email).first()
        if user:
            flash('Email already exists.', category='error')
        elif len(email) < 4:
            flash('Email must be greater than 3 characters.', category='error')
        elif len(user_name) < 2:
            flash('User name must be greater than 1 character.', category='error')
        elif password1 != password2:
            flash('Passwords don\'t match.', category='error')
        elif len(password1) < 7:
            flash('Password must be at least 7 characters.', category='error')
        else:
            new_user = User(email=email, userName=user_name, password=generate_password_hash(password1, method='sha256'))
            db.session.add(new_user)
            db.session.commit()
            login_user(new_user, remember=True)
            flash('Account created!', category='success')
            return redirect(url_for('views.home'))

    return render_template("sign_up.html", user=current_user)


@auth.route('/add_photo', methods=['GET', 'POST'])
@login_required
def add_photo():
    if request.method == 'POST':
        photo_name = request.form.get('photo_name')
        photo_desc = request.form.get('photo_desc')
        uploaded_img = request.files['uploaded-file']
        image_filename = uploaded_img.filename
        # save the full size image with same filename in user-uploads folder
        # uploaded_img.save(os.path.join(current_app.root_path, UPLOAD_FOLDER, image_filename))

        gallery_file = os.path.join(UPLOAD_FOLDER, 'gallery', image_filename)
        thumb_file = os.path.join(UPLOAD_FOLDER, 'thumbnail', image_filename)
        # image_file = os.path.join(UPLOAD_FOLDER, image_filename)

        # Open the image file
        img = Image.open(uploaded_img)
        # Set the maximum dimension for the resized image
        max_dimension = 1500
        # Get the original width and height of the image
        width, height = img.size
        # Check if either width or height is greater than the maximum dimension
        if width > max_dimension or height > max_dimension:
            # Determine the aspect ratio
            aspect_ratio = width / height
            # Calculate the new width and height while maintaining the aspect ratio
            if width > height:
                new_width = max_dimension
                new_height = int(new_width / aspect_ratio)
            else:
                new_height = max_dimension
                new_width = int(new_height * aspect_ratio)

            # Resize the image
            gallery_img = img.resize((new_width, new_height))
            # Save the resized image
            # print(img.format, gallery_file.size)
            gallery_img.save(os.path.join(current_app.root_path, UPLOAD_FOLDER, 'gallery', image_filename))
        else:
            # Image is already smaller than the maximum dimension
            # save file to gallery and maintain filename
            img.save(os.path.join(current_app.root_path, UPLOAD_FOLDER, 'gallery', image_filename))
        # resize img to thumbnail and save
        img.thumbnail((200, 200))
        img.save(os.path.join(current_app.root_path, UPLOAD_FOLDER, 'thumbnail', image_filename))


        photo = Photo.query.filter_by(photo_name=photo_name).first()
        if photo:
            flash('Photo Name already exists.', category='error')
        elif len(photo_name) < 3:
            flash('Title must be greater than 2 characters.', category='error')
        else:
            new_photo = Photo(photo_name=photo_name,
                              photo_desc=photo_desc,
                              gallery_file=gallery_file,
                              thumb_file=thumb_file,
                              user_id=current_user.id)
            db.session.add(new_photo)
            db.session.commit()
            flash('Photo Added!', category='success')
            return redirect(url_for('views.gallery'))

    return render_template("add_photo.html", user=current_user)


@auth.route('/add_link', methods=['GET', 'POST'])
@login_required
def add_link():
    if request.method == 'POST':
        name = request.form.get('name')
        link = request.form.get('link')

        link_address = Link.query.filter_by(link=link).first()
        if link_address:
            flash('That link already exists.', category='error')
        elif len(name) < 3:
            flash('Description must be greater than 2 characters.', category='error')
        else:
            new_link = Link(name=name, link=link, user_id=current_user.id)
            db.session.add(new_link)
            db.session.commit()
            flash('Link Added!', category='success')
            return redirect(url_for('views.links'))

    return render_template("add_link.html", user=current_user)

