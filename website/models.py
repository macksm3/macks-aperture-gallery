from datetime import datetime
from . import db
from flask_login import UserMixin
from sqlalchemy.sql import func


class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    photo_name = db.Column(db.String(255), unique=True, nullable=False)
    photo_desc = db.Column(db.String(10000))
    thumb_file = db.Column(db.String(255), nullable=False)      # file name and path?
    gallery_file = db.Column(db.String(255), nullable=False)      # file name and path?
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    link = db.Column(db.String(255))
    create_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(10000))
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    userName = db.Column(db.String(150))
    notes = db.relationship('Note')


