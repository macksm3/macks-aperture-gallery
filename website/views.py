from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Note, Link, Photo
from . import db
import json

views = Blueprint('views', __name__)


@views.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    photo_list = Photo.query.filter_by(user_id=current_user.id).order_by(Photo.date_posted.desc())
    if request.method == 'POST':
        note = request.form.get('note')

        if len(note) < 1:
            flash('Note is too short!', category='error')
        else:
            new_note = Note(data=note, user_id=current_user.id)
            db.session.add(new_note)
            db.session.commit()
            flash('Note Added!', category='success')
    return render_template("home.html", user=current_user, photos=photo_list)


@views.route('/delete-note', methods=['POST'])
def delete_note():
    note = json.loads(request.data)
    noteId = note['noteId']
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()

    return jsonify({})


@views.route('/')
@views.route('/gallery')
# @login_required
def gallery():
    page = request.args.get('page', 1, type=int)
    photo_list = Photo.query.order_by(Photo.date_posted.desc()).paginate(page=page, per_page=12)
    return render_template("gallery.html", user=current_user, photos=photo_list)


@views.route('/links')
@login_required
def links():
    link_list = Link.query.all()
    return render_template("links.html", user=current_user, links=link_list)

